﻿package MailRuTest;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RegistrationTest {
  private WebDriver driver;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
  }

  @Test
  public void testRegistration() throws Exception {
	  driver.get("http://e.mail.ru/cgi-bin/signup");
	  driver.findElement(By.xpath("//form/div[2]/span[@class='sig2']/input")).sendKeys("Вася");
	  driver.findElement(By.xpath("//form/div[3]/span[@class='sig2']/input")).sendKeys("Пупкин");
	  new Select(driver.findElement(By.xpath("//form/div[4]/span[@class='sig2']/select[1]"))).selectByValue("17");
	  new Select(driver.findElement(By.xpath("//form/div[4]/span[@class='sig2']/select[2]"))).selectByValue("5");
	  new Select(driver.findElement(By.xpath("//form/div[4]/span[@class='sig2']/select[3]"))).selectByValue("1985");
	  driver.findElement(By.name("your_town")).sendKeys("Одесса");
	  new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form/div[5]/span[2]/div/span/div")));
	  driver.findElement(By.name("your_town")).sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
	  driver.findElement(By.id("man2")).click();
	  driver.findElement(By.xpath("//form/div[7]/span[2]/input")).click();
	  driver.findElement(By.xpath("//form/div[7]/span[2]/div/div/div/span/div/a")).click();
	  String pass = "hgd75HFg_76ghf";
	  driver.findElement(By.xpath("//form/div[8]/span[2]/input")).sendKeys(pass);
	  driver.findElement(By.xpath("//form/div[9]/span[2]/input")).sendKeys(pass);
	  driver.findElement(By.id("noPhoneLink")).click();
	  new Select(driver.findElement(By.name("Password_Question"))).selectByIndex(1);
	  driver.findElement(By.xpath("//form/div[12]/div[4]/span[2]/input")).sendKeys("0123456789");
	  driver.findElement(By.xpath("//form/div[12]/div[5]/span[2]/input")).sendKeys("mooncat@ukr.net");
	  driver.findElement(By.xpath("//form/div[14]/span[2]/div/span/span/span/input")).click();
 }
  
  @After
  public void tearDown() throws Exception {
//    driver.quit(); // Close browser, but we want to see filled form
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
}
